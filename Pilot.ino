Pilot::Pilot(int pinP, int pinE, int pinM) {

	photoResistor = new PhotoResistor(pinP);
	electrode = new Electrode(pinE);
	motor = new Motor(pinM);

}

Pilot::~Pilot() {

}

bool Pilot::ignite(int timeout, int threshold) {

	motor->on();
	electrode->on();
	long time = millis();

	while (photoResistor->getAnalog() < threshold) {

		if (millis() - time >= timeout) {

			electrode->off();
			motor->off();
			return false;

		}

	}

	electrode->off();
	return true;

}

void Pilot::extinguish() {

	motor->off();

}

bool Pilot::isLit(int threshold) {

	if (photoResistor->getAnalog() > threshold) {

		return true;

	}

	return false;

}

bool Pilot::isBroken(int threshold) {

	if (photoResistor->getAnalog() < threshold) {

		return true;

	}

	return false;

}
