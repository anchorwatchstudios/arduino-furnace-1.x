FurnaceDisplay::FurnaceDisplay(int pinRs, int pinEnable, int pin4, int pin5, int pin6, int pin7, int hertz, int w, int h, String v) :
		Display(pinRs, pinEnable, pin4, pin5, pin6, pin7, hertz, w, h) {

	menuLCD = new MenuLCD(pinRs, pinEnable, pin4, pin5, pin6, pin7, w, h);
	menuManager = new MenuManager(menuLCD);
	menuLCD->MenuLCDSetup();
	MenuEntry * p_menuEntryRoot;
	p_menuEntryRoot = new MenuEntry("Start", NULL, start);
	menuManager->addMenuRoot(p_menuEntryRoot);
	menuManager->addSibling(new MenuEntry("Stop", NULL, stop));
	menuManager->addSibling(new MenuEntry("Restart", NULL, restart));
	menuManager->addSibling(new MenuEntry("Settings...", NULL, NULL));
	menuManager->addSibling(new MenuEntry("Back", NULL, toggleMainMenu));
	menuManager->MenuDown();
	menuManager->MenuDown();
	menuManager->MenuDown();
	menuManager->addChild(new MenuEntry("Max oil temp.", NULL, setOilTemp));
	menuManager->addChild(new MenuEntry("Max boiler temp.", NULL, setBoilerTemp));
	menuManager->addChild(new MenuEntry("Pilot timeout", NULL, setPilotTimeout));
	menuManager->addChild(new MenuEntry("Pilot low thresh.", NULL, setPilotLowThreshold));
	menuManager->addChild(new MenuEntry("Pilot run thresh.", NULL, setPilotRunThreshold));
	menuManager->addChild(new MenuEntry("Pilot high thresh.", NULL, setPilotHighThreshold));
	menuManager->addChild(new MenuEntry("Back", (void *) menuManager, MenuEntry_BackCallbackFunc));
	menuManager->SelectRoot();
	tick = 0;
	inMenu = false;
	querying = false;
	queryLabel = "";
	queryValue = 0;
	version = v;
	running = false;

}

FurnaceDisplay::~FurnaceDisplay() {

}

void FurnaceDisplay::drawQuery() {

	lcd->clear();
	lcd->setCursor(0, 0);
	lcd->print(queryLabel);
	lcd->setCursor(0, 1);
	lcd->print(queryValue);

}

void FurnaceDisplay::drawMessage(String message, int d) {

	lcd->clear();
	int offset = (width - message.length()) / 2;
	lcd->setCursor(offset, 1);
	lcd->print(message);
	delay(d);

}

void FurnaceDisplay::drawStatus() {

	lcd->clear();
	drawLine("Arduino Furnace " + version, 0);

	if(running) {

		switch (tick) {

		case 0:
			lcd->setCursor(6, 2);
			lcd->print("Running");
			break;
		case 1:
			lcd->setCursor(6, 2);
			lcd->print("Running.");
			break;
		case 2:
			lcd->setCursor(6, 2);
			lcd->print("Running..");
			break;
		case 3:
			lcd->setCursor(6, 2);
			lcd->print("Running...");
			break;
		}

	}
	else {

		drawLine("Stopped", 2);

	}

	drawLine("***Press for menu***", 3);

}

void FurnaceDisplay::drawLine(String message, int line) {

	int offset = (width - message.length()) / 2;
	lcd->setCursor(offset, line);
	lcd->print(message);

}

void FurnaceDisplay::drawMenu() {

	menuManager->DrawMenu();

}

void FurnaceDisplay::render() {

	if (querying) {

		drawQuery();

	}
	else if (inMenu) {

		drawMenu();

	}
	else {

		drawStatus();

	}

	tick++;

	if(tick == 4) {

		tick = 0;

	}

}

void FurnaceDisplay::toggleMenu() {

	menuManager->SelectRoot();
	inMenu = !inMenu;

}


bool FurnaceDisplay::isInMenu() {

	return inMenu;

}

void FurnaceDisplay::toggleQuery() {

	querying = !querying;

}

bool FurnaceDisplay::isQuerying() {

	return querying;

}

void FurnaceDisplay::setQueryLabel(String label) {

	queryLabel = label;

}

void FurnaceDisplay::setQueryValue(int value) {

	queryValue = value;

}

void FurnaceDisplay::menuUp() {

	menuManager->MenuUp();

}

void FurnaceDisplay::menuDown() {

	menuManager->MenuDown();

}

void FurnaceDisplay::menuSelect() {

	menuManager->MenuSelect();

}

void FurnaceDisplay::setRunning(bool r) {

	running = r;

}

