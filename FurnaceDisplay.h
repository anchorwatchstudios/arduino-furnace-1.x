#ifndef FURNACE_DISPLAY_H
#define FURNACE_DISPLAY_H

#include <MenuLCD.h>
#include <MenuManager.h>
#include <MenuEntry.h>
#include "Display.h"

/**
 * This class extends the Display class and adds drawing
 * functionality and variables specific to a furnace.
 */
class FurnaceDisplay : public Display {

public:

	/**
	 * Constructs a new FurnaceDisplay with the given pins,
	 * HERTZ, width, height, and version string.
	 *
	 * @param pinRs
	 *   The LCD_RS pin
	 * @param pinEnable
	 *   The LCD_ENABLE pin
	 * @param pin4
	 *   The LCD_4 pin
	 * @param pin5
	 *   The LCD_5 pin
	 * @param pin6
	 *   The LCD_6 pin
	 * @param pin7
	 *   The LCD_7 pin
	 * @param hertz
	 *   The LCD refresh rate, in HERTZ
	 * @param w
	 *   The number of columns on the LCD
	 * @param h
	 *   The number of rows on the LCD
	 * @param v
	 *   The version of the furnace software to display
	 */
	FurnaceDisplay(int pinRs, int pinEnable, int pin4, int pin5, int pin6, int pin7, int hertz, int w, int h, String v);

	/**
	 * Destroys the FurnaceDisplay.
	 */
	~FurnaceDisplay();

	/**
	 * Draws the given message and then pauses
	 * for the given delay.
	 *
	 * @param message
	 *   The message to display
	 * @param d
	 *   the time in milliseconds to show the message
	 */
	void drawMessage(String message, int d);

	/**
	 * Renders the display. Not to be called by operator.
	 */
	void render();

	/**
	 * Toggles the menu.
	 */
	void toggleMenu();

	/**
	 * Returns true if the user is in the menu
	 *
	 * @return
	 *   True if user is in the menu
	 */
	bool isInMenu();

	/**
	 * Toggles whether or not the user is being
	 * queried.
	 */
	void toggleQuery();

	/**
	 * Returns true if the user is being queried.
	 *
	 * @return
	 *   True if the user is being queried
	 */
	bool isQuerying();

	/**
	 * Sets the label to be shown when querying
	 * the user.
	 *
	 * @param label
	 *   The label to show when querying the user
	 */
	void setQueryLabel(String label);

	/**
	 * Sets the value to be shown when querying
	 * the user.
	 *
	 * @param value
	 *   The value to be shown when querying the user
	 */
	void setQueryValue(int value);

	/**
	 * Sets whether or not the system is running.
	 *
	 * @param r
	 *   Whether or not the system is running
	 */
	void setRunning(bool r);

	/**
	 * Moves the menu cursor up.
	 */
	void menuUp();

	/**
	 * Moves the menu cursor down.
	 */
	void menuDown();

	/**
	 * Selects the current item in the menu.
	 */
	void menuSelect();

private:

	String version;
	int tick;
	bool inMenu;
	bool querying;
	bool running;
	String queryLabel;
	int queryValue;
	MenuManager *menuManager;
	MenuLCD *menuLCD;

	/**
	 * Draws a message centered on the given line.
	 *
	 * @param message
	 *   The message to be drawn
	 * @param line
	 *   The row of the LCD to draw it on.
	 */
	void drawLine(String message, int line);

	/**
	 * Draws the user query.
	 */
	void drawQuery();

	/**
	 * Draws the status of the system.
	 */
	void drawStatus();

	/**
	 * Draws the menu.
	 */
	void drawMenu();

};

#endif
