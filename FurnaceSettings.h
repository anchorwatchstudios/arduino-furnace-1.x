#ifndef FURNACE_SETTINGS_H
#define FURNACE_SETTINGS_H

/**
 * A convenience class for holding settings variables.
 */
class FurnaceSettings {

public:

	/**
	 * constructs a new instance of FurnaceSettings with
	 * the default values.
	 */
	FurnaceSettings();

	/**
	 * Constructs a new instance of FurnaceSettings with
	 * the given values.
	 *
	 * @param bTemp
	 *   The maximum boiler temperature
	 * @param oTemp
	 *   The maximum oil temperature
	 * @param pTimeout
	 *   The pilot timeout in milliseconds
	 * @param pLow
	 *   The pilot photoresistor low threshold (raw)
	 * @param pHigh
	 *   The pilot photoresistor high threshold (raw)
	 * @param pRun
	 *   The pilot photoresistor run threshold (raw)
	 */
	FurnaceSettings(int bTemp, int oTemp, int pTimeout, int pLow, int pHigh, int pRun);

	/**
	 * Destroys this instance of FurnaceSettings.
	 */
	~FurnaceSettings();

	int boilerTemp;
	int oilTemp;
	int pilotTimeout;
	int pilotLowThreshold;
	int pilotHighThreshold;
	int pilotRunThreshold;

};

#endif
