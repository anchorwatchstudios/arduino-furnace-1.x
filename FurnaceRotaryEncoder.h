#ifndef FURNACE_ROTARY_ENCODER_H
#define FURNACE_ROTARY_ENCODER_H

#include "RotaryEncoder.h"
#include "FurnaceDisplay.h"

/**
 * This class extends the RotaryEncoder class with query
 * functionality, and defines actions for the abstract
 * events provided by RotaryEncoder. This class can store
 * an integer value queried from the user and save this
 * value in the given pointer location.
 */
class FurnaceRotaryEncoder : public RotaryEncoder {

public:

	/**
	 * Constructs a new FurnaceRotaryEncoder with the given
	 * interrupt pins and FurnaceDisplay.
	 *
	 * @param pinButton
	 *   The button pin
	 * @param pinInterruptA
	 *   The left interrupt pin for the rotary encoder
	 * @param pinInterruptB
	 *   The right interrupt pin for the rotary encoder
	 * @param *d
	 *   A pointer to a FurnaceDisplay
	 */
	FurnaceRotaryEncoder(int pinButton, int pinInterruptA, int pinInterruptB, FurnaceDisplay *d);

	/**
	 * Called when the rotary dial turns to the right.
	 */
	void onIncrement();

	/**
	 * Called when the rotary dial turns to the left.
	 */
	void onDecrement();

	/**
	 * Called when the rotary dial is pushed down.
	 */
	void onButtonDown();

	/**
	 * Called when the rotary dial is not pushed down.
	 */
	void onButtonReleased();

	/**
	 * Called when the rotary dial is pressed and then
	 * released.
	 */
	void onButtonPressed();

	/**
	 * Returns the value queried from the user.
	 *
	 * @return
	 *   The value queried from the user
	 */
	int getQueryValue();

	/**
	 * Sets the value being queried by the user.
	 *
	 * @param value
	 *   The initial query value
	 */
	void setQueryValue(int value);

	/**
	 * Sets the pointer to the data to alter
	 * when saving the query data.
	 *
	 * @param *pointer
	 *   A pointer to the data
	 */
	void setQueryPointer(int *pointer);

private:

	int queryValue;
	int *queryPointer;
	FurnaceDisplay *display;

};

#endif
