FurnaceSettings::FurnaceSettings() {

	boilerTemp = 25;
	oilTemp = 25;
	pilotTimeout = 5000;
	pilotLowThreshold = 20;
	pilotHighThreshold = 1000;
	pilotRunThreshold = 600;

}

FurnaceSettings::FurnaceSettings(int bTemp, int oTemp, int pTimeout, int pLow, int pHigh, int pRun) {

	boilerTemp = bTemp;
	oilTemp = oTemp;
	pilotTimeout = pTimeout;
	pilotLowThreshold = pLow;
	pilotHighThreshold = pHigh;
	pilotRunThreshold = pRun;

}

FurnaceSettings::~FurnaceSettings() {

}
