#ifndef PREHEATER_H
#define PREHEATER_H

/**
 * Represents an oil or air preheater.
 */
class Preheater {

public:

	/**
	 * Constructs a new Preheater with the given pin.
	 *
	 * @param p
	 *   The preheater pin
	 */
	Preheater(int p);

	/**
	 * Destroys the Preheater.
	 */
	~Preheater();

	/**
	 * Returns true if the Preheater is on.
	 *
	 * @return
	 *   True if the Preheater is on.
	 */
	bool isOn();

	/**
	 * Turns the Preheater on.
	 */
	void on();

	/**
	 * Turns the Preheater off.
	 */
	void off();

private:

	int pin;
	bool isHigh;

};

#endif
