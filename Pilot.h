#ifndef PILOT_H
#define PILOT_H

#include "PhotoResistor.h"
#include "Electrode.h"
#include "Motor.h"

/**
 * Represents a pilot flame.
 */
class Pilot {

public:

	/**
	 * Constructs a new Pilot with the given pins.
	 *
	 * @param pinP
	 *   The photoresistor pin
	 * @param pinE
	 *   The electrode pin
	 * @param pinM
	 *   The motor pin
	 */
	Pilot(int pinP, int pinE, int pinM);

	/**
	 * Destroys the Pilot.
	 */
	~Pilot();

	/**
	 * Ignites the pilot using the given timeout and
	 * photoresistor threshold.
	 *
	 * @param timeout
	 *   The pilot timeout
	 * @param threshold
	 *   The photoresistor threshold
	 */
	bool ignite(int timeout, int threshold);

	/**
	 * Extinguishes the pilot.
	 */
	void extinguish();

	/**
	 * Returns true if the pilot is lit.
	 *
	 * @param threshold
	 *   The photoresistor threshold
	 * @return
	 *   True if the pilot is lit
	 */
	bool isLit(int threshold);

	/**
	 * Returns true if the pilot is broken.
	 *
	 * @param threshold
	 *   The photoresistor threshold
	 * @return
	 *   True if the pilot is broken
	 */
	bool isBroken(int threshold);

private:

	PhotoResistor *photoResistor;
	Electrode *electrode;
	Motor *motor;

};

#endif
