/**
 * This program controls a furnace using an Arduino Uno
 * microcontroller or similar device with an LCD display
 * and rotary encoder attached.
 *
 * @author Andrew Szurley
 * @author Anthony Atella
 */
#include "FurnaceDisplay.h"
#include "FurnaceRotaryEncoder.h"
#include "FurnaceSettings.h"
#include "Pilot.h"
#include "Preheater.h"
#include "TempSensor.h"

#define PIN_LOCATION_OIL_TEMP A1
#define PIN_LOCATION_BOILER_TEMP A0
#define PIN_LOCATION_PILOT A2
#define PIN_LOCATION_PREHEATER 10
#define PIN_LOCATION_ELECTRODE 9
#define PIN_LOCATION_MOTOR 13
#define PIN_LOCATION_INTERRUPT_A 2
#define PIN_LOCATION_INTERRUPT_B 3
#define PIN_LOCATION_BUTTON 8
#define PIN_LOCATION_LCD_RS 12
#define PIN_LOCATION_LCD_ENABLE 11
#define PIN_LOCATION_LCD_4 5
#define PIN_LOCATION_LCD_5 4
#define PIN_LOCATION_LCD_6 6
#define PIN_LOCATION_LCD_7 7
#define LCD_WIDTH 20
#define LCD_HEIGHT 4
#define LCD_HZ 5
#define MESSAGE_DELAY 1000
#define SPLASH_DELAY 5000
#define ROTARY_SPEED 1

static String VERSION = "1.3";
static FurnaceDisplay LCD_DISPLAY(
		PIN_LOCATION_LCD_RS,
		PIN_LOCATION_LCD_ENABLE,
		PIN_LOCATION_LCD_4,
		PIN_LOCATION_LCD_5,
		PIN_LOCATION_LCD_6,
		PIN_LOCATION_LCD_7,
		LCD_HZ,
		LCD_WIDTH,
		LCD_HEIGHT,
		VERSION);
static FurnaceRotaryEncoder ROTARY_ENCODER(
		PIN_LOCATION_BUTTON,
		PIN_LOCATION_INTERRUPT_A,
		PIN_LOCATION_INTERRUPT_B,
		&LCD_DISPLAY);
static TempSensor OIL_TEMP_SENSOR(PIN_LOCATION_OIL_TEMP);
static TempSensor BOILER_TEMP_SENSOR(PIN_LOCATION_BOILER_TEMP);
static Pilot PILOT(PIN_LOCATION_PILOT, PIN_LOCATION_ELECTRODE, PIN_LOCATION_MOTOR);
static Preheater OIL_PREHEATER(PIN_LOCATION_PREHEATER);
static FurnaceSettings SETTINGS;
bool running = false;

/**
   Called when the the power or reset button is pressed
   on the board. Displays a splash message.
 */
void setup() {

	LCD_DISPLAY.drawMessage("Hello", SPLASH_DELAY);

}

/**
 * The main program loop. Each iteration of the loop consists of
 * checking for safety, deciding which state to be in, and updating
 * input and then output components, in that order.
 */
void loop() {

	checkSafety();
	checkState();
	ROTARY_ENCODER.update();
	LCD_DISPLAY.setRunning(running);
	LCD_DISPLAY.setQueryValue(ROTARY_ENCODER.getQueryValue());
	LCD_DISPLAY.update();

}

/**
 * A subroutine for starting the furnace. First, the preheater
 * must reach the target temperature, then the pilot must ignite.
 * If either process times out or fails the system calls the stop()
 * subroutine, and an error message is displayed.
 */
void start() {

	// TODO: Fix start logic

	if (running) {

		LCD_DISPLAY.drawMessage("Already running", MESSAGE_DELAY);

	}
	else {

		LCD_DISPLAY.drawMessage("Starting...", MESSAGE_DELAY);
		running = true;
		OIL_PREHEATER.on();
		LCD_DISPLAY.drawMessage("Waiting for pilot...", MESSAGE_DELAY);

		if (PILOT.ignite(SETTINGS.pilotTimeout, SETTINGS.pilotRunThreshold)) {

			LCD_DISPLAY.toggleMenu();

		}
		else {

			OIL_PREHEATER.off();
			LCD_DISPLAY.drawMessage("Pilot won't start!", MESSAGE_DELAY);
			stop();

		}


	}

}

/**
 * A subroutine for stopping the system and all of its components.
 * When this method is executed a message is displayed, the
 * preheater is turned off, and the pilot is extinguished.
 */
void stop() {

	// TODO: Verify with Andy

	LCD_DISPLAY.drawMessage("Shutting down...", MESSAGE_DELAY);
	OIL_PREHEATER.off();
	PILOT.extinguish();
	running = false;

}

/**
 * A delegate method for stopping and starting the system.
 */
void restart() {

	stop();
	start();

}

/**
 * A subroutine that checks the overall safety of the system.
 * The system displays a message and calls the stop() subroutine
 * if any tests fail. This method tests for temperature
 * tolerances in the boiler as well as the preheater. It also
 * checks the validity of the pilot sensor and ensures that the
 * pilot stays on.
 */
void checkSafety() {

	// TODO: Verify with Andy

	if (running) {

		if (BOILER_TEMP_SENSOR.getTemp() > SETTINGS.boilerTemp) {

			LCD_DISPLAY.drawMessage("Boiler is too hot!", MESSAGE_DELAY);
			stop();

		}
		else if (OIL_TEMP_SENSOR.getTemp() > SETTINGS.oilTemp) {

			LCD_DISPLAY.drawMessage("Oil is too hot!", MESSAGE_DELAY);
			stop();

		}
		else if (PILOT.isBroken(SETTINGS.pilotLowThreshold)) {

			LCD_DISPLAY.drawMessage("Pilot is broken!", MESSAGE_DELAY);
			stop();

		}
		else if (!PILOT.isLit(SETTINGS.pilotRunThreshold)) {

			LCD_DISPLAY.drawMessage("Pilot is out!", MESSAGE_DELAY);
			stop();

		}

	}

}

/**
 * A subroutine that checks the current state of the system
 * and determines if the pilot need to be on or off to reach
 * the target temperature.
 */
void checkState() {

	// TODO: Check state

}

/**
 * A delegate method to open and close the menu. This method
 * is needed for the menu system to function.
 */
void toggleMainMenu() {

	LCD_DISPLAY.toggleMenu();

}

/**
 * A delegate method to set the oil temperature. This method
 * is needed for the menu system to function.
 */
void setOilTemp() {

	LCD_DISPLAY.setQueryLabel("Max. Oil Temp.:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.oilTemp);
	ROTARY_ENCODER.setQueryValue(SETTINGS.oilTemp);
	LCD_DISPLAY.toggleQuery();

}

/**
 * A delegate method to set the boiler temperature. This method
 * is needed for the menu system to function.
 */
void setBoilerTemp() {

	LCD_DISPLAY.setQueryLabel("Max. Boiler Temp.:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.boilerTemp);
	ROTARY_ENCODER.setQueryValue(SETTINGS.boilerTemp);
	LCD_DISPLAY.toggleQuery();

}

/**
 * A delegate method to set the pilot timeout. This method
 * is needed for the menu system to function.
 */
void setPilotTimeout() {

	LCD_DISPLAY.setQueryLabel("Pilot Timeout:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.pilotTimeout);
	ROTARY_ENCODER.setQueryValue(SETTINGS.pilotTimeout);
	LCD_DISPLAY.toggleQuery();

}

/**
 * A delegate method to set the pilot low threshold. This method
 * is needed for the menu system to function.
 */
void setPilotLowThreshold() {

	LCD_DISPLAY.setQueryLabel("Pilot Low Thresh.:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.pilotLowThreshold);
	ROTARY_ENCODER.setQueryValue(SETTINGS.pilotLowThreshold);
	LCD_DISPLAY.toggleQuery();

}

/**
 * A delegate method to set the pilot run threshold. This method
 * is needed for the menu system to function.
 */
void setPilotRunThreshold() {

	LCD_DISPLAY.setQueryLabel("Pilot Run Thresh.:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.pilotRunThreshold);
	ROTARY_ENCODER.setQueryValue(SETTINGS.pilotRunThreshold);
	LCD_DISPLAY.toggleQuery();

}

/**
 * A delegate method to set the pilot high threshold. This method
 * is needed for the menu system to function.
 */
void setPilotHighThreshold() {

	LCD_DISPLAY.setQueryLabel("Pilot High Thresh.:");
	ROTARY_ENCODER.setQueryPointer(&SETTINGS.pilotHighThreshold);
	ROTARY_ENCODER.setQueryValue(SETTINGS.pilotHighThreshold);
	LCD_DISPLAY.toggleQuery();

}
