#ifndef ELECTRODE_H
#define ELECTRODE_H

/**
 * Represents an electrode.
 */
class Electrode {

public:

	/**
	 * Constructs a new Electrode with the given pin.
	 *
	 * @param p
	 *   The electrode pin
	 */
	Electrode(int p);

	/**
	 * Destroys the Electrode.
	 */
	~Electrode();

	/**
	 * Returns true if the Electrode is on.
	 *
	 * @return
	 *   True if the Electrode is on.
	 */
	bool isOn();

	/**
	 * Turns on the Electrode.
	 */
	void on();

	/**
	 * Turns off the Electrode.
	 */
	void off();

private:

	int pin;
	bool isHigh;

};

#endif
