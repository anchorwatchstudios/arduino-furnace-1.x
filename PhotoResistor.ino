PhotoResistor::PhotoResistor(int p) {

	pin = p;
	pinMode(pin, INPUT);

}

PhotoResistor::~PhotoResistor() {

}

double PhotoResistor::analogToLux(int analog) {

	// TODO: Broken???
	double vOut = (double) analog * 0.0048828125;
	// use this equation if the LDR is in the upper part of the divider
	return 500 / (10 * ((5 - vOut) / vOut));
	// return (2500 / vOut - 500) / 10;

}

int PhotoResistor::getAnalog() {

	return analogRead(pin);

}

double PhotoResistor::getLux() {

	return analogToLux(digitalRead(pin));

}
