#ifndef MOTOR_H
#define MOTOR_H

/**
 * Represents a motor.
 */
class Motor {

public:

	/**
	 * Constructs a new Motor with the given pin.
	 *
	 * @param p
	 *   The motor pin
	 */
	Motor(int p);

	/**
	 * Destroys the Motor.
	 */
	~Motor();

	/**
	 * Returns true if the Motor is on.
	 *
	 * @return
	 *   True if the motor is on
	 */
	bool isOn();

	/**
	 * Turns the Motor on.
	 */
	void on();

	/**
	 * Turns the motor off.
	 */
	void off();

private:

	int pin;
	bool isHigh;

};

#endif
