Electrode::Electrode(int p) {

	pin = p;
	pinMode(pin, OUTPUT);
	isHigh = false;

}

Electrode::~Electrode() {

}

bool Electrode::isOn() {

	return isHigh;
}

void Electrode::on() {

	digitalWrite(pin, HIGH);
	isHigh = true;

}

void Electrode::off() {

	digitalWrite(pin, LOW);
	isHigh = false;

}
