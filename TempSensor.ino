TempSensor::TempSensor(int p) {

	pin = p;
	pinMode(pin, INPUT);

}

TempSensor::~TempSensor() {

}

double TempSensor::getTemp() {

	return analogToTemp(analogRead(pin));

}

double TempSensor::analogToTemp(int analog) {

	double voltage = ((double) analog / 1024.0) * 5.0;
	return (voltage - .5) * 100.0;

}
