Motor::Motor(int p) {

	pin = p;
	pinMode(pin, OUTPUT);
	isHigh = false;

}

Motor::~Motor() {

}

bool Motor::isOn() {

	return isHigh;
}

void Motor::on() {

	digitalWrite(pin, HIGH);
	isHigh = true;

}

void Motor::off() {

	digitalWrite(pin, LOW);
	isHigh = false;

}
