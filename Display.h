#ifndef DISPLAY_H
#define DISPLAY_H

#include <LiquidCrystal.h>

/**
 * Represents a LiquidCrystal display that can update based on
 * the given HERTZ. Implementations of Display should provide
 * the render() method with the appropriate drawing code. The
 * update() function should be called each time the loop iterates.
 */
class Display {

public:

	/**
	 * Constructs a new Display with the given pins, hertz, width,
	 * and height.
	 *
	 * @param pinRs
	 *   The LCD_RESET pin
	 * @param pinEnable
	 *   The LCD_ENABLE pin
	 * @param pin4
	 *   The LCD_4 pin
	 * @param pin5
	 *   The LCD_5 pin
	 * @param pin6
	 *   The LCD_6 pin
	 * @param pin7
	 *   The LCD_7 pin
	 * @param hertz
	 *   The LCD render speed, in HERTZ
	 * @param width
	 *   The number of columns on the LCD
	 * @param height
	 *   The number of rows on the LCD
	 */
	Display(int pinRs, int pinEnable, int pin4, int pin5, int pin6, int pin7, int hertz, int w, int h);

	/**
	 * Destroys the Display.
	 */
	~Display();

	/**
	 * Updates the Display. When this method
	 * is called the difference between the last
	 * render and now is measured against the
	 * given HERTZ, and render is called if enough
	 * time has passed.
	 */
	void update();

	/**
	 * Called when the update() method has calculated
	 * that enough time has passed. This method should
	 * not be called by an operator.
	 */
	virtual void render();

protected:

	int hz;
	long lastRender;
	int width;
	int height;
	LiquidCrystal *lcd;

};

#endif
