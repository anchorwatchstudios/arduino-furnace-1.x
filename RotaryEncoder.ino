RotaryEncoder::RotaryEncoder(int pinButton, int pinInterruptA, int pinInterruptB) {

	button.setup(pinButton);
	encoder = new Encoder(pinInterruptA, pinInterruptB);
	buttonPressed = false;
	speed = 2;
	lastRotaryValue = 0;

}

RotaryEncoder::~RotaryEncoder() {

}

void RotaryEncoder::update() {

	if (button.process(millis())) {

		buttonPressed = true;
		onButtonDown();

	}
	else {

		onButtonReleased();

		if(buttonPressed == true) {

			buttonPressed = false;
			onButtonPressed();

		}

	}

	int currentRotaryValue = encoder->read();

	if ((lastRotaryValue - currentRotaryValue) <= -(speed * 2)) {

		lastRotaryValue = currentRotaryValue;
		onDecrement();

	}
	else if ((lastRotaryValue - currentRotaryValue) >= (speed * 2)) {

		lastRotaryValue = currentRotaryValue;
		onIncrement();

	}

}

int RotaryEncoder::getSpeed() {

	return speed;

}

void RotaryEncoder::setSpeed(int s) {

	speed = s;

}

void RotaryEncoder::onDecrement() {

}

void RotaryEncoder::onIncrement() {

}

void RotaryEncoder::onButtonDown() {

}

void RotaryEncoder::onButtonReleased() {

}

void RotaryEncoder::onButtonPressed() {

}
