FurnaceRotaryEncoder::FurnaceRotaryEncoder(int pinButton, int pinInterruptA, int pinInterruptB, FurnaceDisplay *d) :
		RotaryEncoder(pinButton, pinInterruptA, pinInterruptB) {

	display = d;
	queryValue = 0;

}

void FurnaceRotaryEncoder::onDecrement() {

	if (display->isQuerying()) {

		queryValue--;

	}
	else if (display->isInMenu()) {

		display->menuUp();

	}

}

void FurnaceRotaryEncoder::onIncrement() {

	if (display->isQuerying()) {

		queryValue++;

	}
	else if (display->isInMenu()) {

		display->menuDown();

	}

}

void FurnaceRotaryEncoder::onButtonDown() {

}

void FurnaceRotaryEncoder::onButtonReleased() {

}

void FurnaceRotaryEncoder::onButtonPressed() {

	if (display->isQuerying()) {

		*queryPointer = queryValue;
		display->toggleQuery();

	}
	else if (display->isInMenu()) {

		display->menuSelect();

	}
	else {

		display->toggleMenu();

	}

}

int FurnaceRotaryEncoder::getQueryValue() {

	return queryValue;

}

void FurnaceRotaryEncoder::setQueryValue(int value) {

	queryValue = value;

}

void FurnaceRotaryEncoder::setQueryPointer(int *pointer) {

	queryPointer = pointer;
	queryValue = queryPointer;

}
