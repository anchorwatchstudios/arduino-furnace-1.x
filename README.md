![Alt logo](https://bitbucket.org/anchorwatchstudios/arduino-furnace-1.x/avatar/512/)

# Arduino Furnace 1.x #

My buddy purchased an Arduino Uno microcontroller and wanted to build an oil furnace as a little pet project. He was quickly overwhelmed with the size of his script, and as it grew it became more cryptic. He asked me to take a look at it to see if I could make it easier to understand. So I took his script and rewrote it to take an object-oriented approach. The project fizzled out and we stopped collaborating because he ended up purchasing a furnace in the long run. This software was never tested on an actual furnace. The project was never finished.

# WARNING! RUN THIS SOFTWARE AT YOUR OWN RISK! #

## Setup ##

You will need certain hardware components, software, and dependencies to run this software.

### Hardware ###

* Arduino Uno
* Arduino USB
* Reprap LCD2004 Smart Controller Display
* Photoresistor ("Sees" the flame)
* Electrode (Spark to light pilot)
* Temperature sensor
* LED (To represent the oil pre-heater power)
* Breadboard
* Wires and connectors
* A lighter

### Software ###

* [Arduino Software (IDE)](https://www.arduino.cc/en/Main/Software)

### Dependencies ###

* [LiquidCrystalDisplay](https://www.arduinolibraries.info/libraries/liquid-crystal)
* [Arduino LCD Menu](https://github.com/DavidAndrews/Arduino_LCD_Menu)
* [Encoder](https://www.arduinolibraries.info/libraries/encoder)
* [InputDebounce](https://www.arduinolibraries.info/libraries/input-debounce)

### Setup ###

* Attach your Arduino Uno to the computer via USB
* Open Arduino Software
* Clone this project into your workspace
* Open the project
* Download dependencies
* Upload to your device

## Tests ##

* Try to start the furnace using the lighter as the pilot
* See what happens when the pilot goes out
* See what happens when the pilot is too dim
* See what happens when the pilot doesn't light
* Edit settings in the menu

## Contribution guidelines ##

This project is no longer being worked on, but feel free to use or modify it AT YOUR OWN RISK.

## Who do I talk to? ##

* [Anthony Atella](https://bitbucket.org/AnthonyAtella/)