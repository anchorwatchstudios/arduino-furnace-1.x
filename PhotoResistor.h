#ifndef PHOTO_RESISTOR_H
#define PHOTO_RESISTOR_H

/**
 * Represents a photoresistor.
 */
class PhotoResistor {

public:

	/**
	 * Constructs a new PhotoResistor with the given pin.
	 *
	 * @param p
	 *   The photoresistor pin
	 */
	PhotoResistor(int p);

	/**
	 * Destroys the PhotoResistor.
	 */
	~PhotoResistor();

	/**
	 * Returns the analog reading from the
	 * photoresistor pin.
	 *
	 * @return
	 *   The analog reading from the photoresistor pin
	 */
	int getAnalog();

	/**
	 * Returns the reading from the photoresistor
	 * pin in lux.
	 *
	 * @return
	 *   The lux reading from the photoresistor pin
	 */
	double getLux();

private:

	int pin;

	/**
	 * Converts the analog reading from the photoresistor
	 * pin to lux.
	 *
	 * @param analog
	 *   The analog reading from the photoresistor pin
	 * @return
	 *   The lux reading from the photoresistor pin
	 */
	double analogToLux(int analog);

};

#endif
