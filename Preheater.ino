Preheater::Preheater(int p) {

	pin = p;
	pinMode(pin, OUTPUT);
	isHigh = false;

}

Preheater::~Preheater() {

}

bool Preheater::isOn() {

	return isHigh;
}

void Preheater::on() {

	digitalWrite(pin, HIGH);
	isHigh = true;

}

void Preheater::off() {

	digitalWrite(pin, LOW);
	isHigh = false;

}
