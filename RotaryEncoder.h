#ifndef ROTARY_ENCODER_H
#define ROTARY_ENCODER_H

#include <Encoder.h>
#include <InputDebounce.h>

/**
 * Represents a rotary encoder. This class relies on the
 * InputDebounce library as well as the Encoder library
 * to handle input debouncing and reading the signal from
 * the rotary encoder dial. This class calls abstract events
 * when dial or button events occur. Sub-classes of this class
 * should define these methods.
 */
class RotaryEncoder {

public:

	/**
	 * Constructs a new RotaryEncoder with the given pins.
	 *
	 * @param pinButton
	 *   The button pin
	 * @param pinInterruptA
	 *   The left interrupt pin for the rotary encoder dial
	 * @param pinInterruptB
	 *   The right interrupt pin for the rotary encoder dial
	 */
	RotaryEncoder(int pinButton, int pinInterruptA, int pinInterruptB);

	/**
	 * Destroys the RotaryEncoder.
	 */
	~RotaryEncoder();

	/**
	 * Updates the state of the RotaryEncoder. This method should
	 * be called each time the loop iterates. This method checks
	 * for button presses and dial turning.
	 */
	void update();

	/**
	 * Sets the speed of the dial. The speed is inverse; the
	 * higher it goes, the slower it is. One is the fastest value,
	 * and each value after that causes the dial to need to be turned
	 * again. For example, if s = 3, the dial would have to click 3
	 * times before update() would register a dial turn.
	 *
	 * @param s
	 *   The speed of the dial
	 */
	void setSpeed(int s);

	/**
	 * Returns the speed of the RotaryEncoder dial.
	 *
	 * @return
	 *   The speed of the rotary encoder dial
	 */
	int getSpeed();

	/**
	 * Called when the dial is turned to the left.
	 */
	virtual void onDecrement();

	/**
	 * Called when the dial is turned to the right.
	 */
	virtual void onIncrement();

	/**
	 * Called when the button is down.
	 */
	virtual void onButtonDown();

	/**
	 * Called when the button is up.
	 */
	virtual void onButtonReleased();

	/**
	 * Called when the button is pressed and then released.
	 */
	virtual void onButtonPressed();

private:

	InputDebounce button;
	Encoder *encoder;
	bool buttonPressed;
	int lastRotaryValue;
	int speed;

};

#endif
