Display::Display(int pinRS, int pinEnable, int pin4, int pin5, int pin6, int pin7, int hertz, int w, int h) {

	lcd = new LiquidCrystal(pinRS, pinEnable, pin4, pin5, pin6, pin7);
	lcd->begin(w, h);
	hz = hertz;
	lastRender = 0;
	width = w;
	height = h;

}

Display::~Display() {

}

void Display::update() {

	long now = millis();

	if ((now - lastRender) >= 1000 / hz) {

		render();
		lastRender = now;

	}

}

void Display::render() {

}
