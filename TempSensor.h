#ifndef TEMP_SENSOR_H
#define TEMP_SENSOR_H

/**
 * Repressents a temperature sensor.
 */
class TempSensor {

public:

	/**
	 * Constructs a new TempSensor with the given pin.
	 *
	 * @param p
	 *   The temperature sensor pin
	 */
	TempSensor(int p);

	/**
	 * Destroys the TempSensor.
	 */
	~TempSensor();

	/**
	 * Returns the temperature, in degrees celsius (?)
	 * read from the temperature sensor pin.
	 *
	 * @return
	 *   The temperature in degrees celsius (?)
	 */
	double getTemp();

private:

	int pin;

	/**
	 * Converts an analog signal to a temperature, in
	 * degrees celsius (?).
	 *
	 * @param analog
	 *   The analog signal from the temperature sensor
	 * @return
	 *   The temperature in degrees celsius (?)
	 */
	double analogToTemp(int analog);

};

#endif
